package com.example.lint.checks

import com.android.tools.lint.detector.api.*
import com.intellij.psi.PsiMethod
import org.jetbrains.annotations.NotNull
import org.jetbrains.uast.UCallExpression

@Suppress("UnstableApiUsage")
class LogDetector : Detector(), Detector.UastScanner {
    override fun getApplicableMethodNames(): List<String>? {
        return arrayListOf("v", "d", "i", "w", "e", "wtf")
    }

    override fun visitMethodCall(@NotNull context: JavaContext,
                                 @NotNull node: UCallExpression,
                                 @NotNull method: PsiMethod
    ) {
        if (context.evaluator.isMemberInClass(method, "android.util.Log")) {
            context.report(ISSUE, node, context.getLocation(node), "避免调用android.util.Log")
        }
    }

    companion object {
        val ISSUE = Issue.create(
            "LogUsage",
            "避免调用android.util.Log",
            "请勿直接调用android.util.Log，应该使用统一工具类",
            Category.SECURITY, 5, Severity.WARNING,
            Implementation(LogDetector::class.java, Scope.JAVA_FILE_SCOPE)
        )
    }
}