package com.example.lint.checks

import com.android.tools.lint.detector.api.*
import com.intellij.psi.PsiMethod
import org.jetbrains.annotations.NotNull
import org.jetbrains.uast.UCallExpression
import java.util.*


@Suppress("UnstableApiUsage")
class NewThreadDetector : Detector(), Detector.UastScanner {


    override fun getApplicableConstructorTypes(): List<String>? {
        return Collections.singletonList("java.lang.Thread")
    }

    override fun visitConstructor(
        @NotNull context: JavaContext,
        @NotNull node: UCallExpression,
        @NotNull constructor: PsiMethod
    ) {
        context.report(
            ISSUE, node, context.getLocation(node),
            "避免自己创建Thread"
        )
    }

    companion object {

        val ISSUE = Issue.create(
            "NewThread",
            "避免自己创建Thread",
            "请勿直接调用new Thread()，建议使用统一的线程管理工具类",
            Category.PERFORMANCE, 5, Severity.WARNING,
            Implementation(NewThreadDetector::class.java, Scope.JAVA_FILE_SCOPE)
        )
    }
}