/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.lint.checks

import com.android.tools.lint.checks.infrastructure.LintDetectorTest
import com.android.tools.lint.detector.api.Detector
import com.android.tools.lint.detector.api.Issue

@Suppress("UnstableApiUsage")
class NewThreadCodeDetectorTest : LintDetectorTest() {
    fun testBasic() {
        lint().files(
            java(
                """
                    package test.pkg;
                    public class TestClass1 {
                        public void test(){
                            new Thread();
                        }
                    }
                    """
            ).indented()
        ).allowMissingSdk()
            .run()
            .expect(
                """
                    src/test/pkg/TestClass1.java:4: Warning: 避免自己创建Thread [NewThread]
                            new Thread();
                            ~~~~~~~~~~~~
                    0 errors, 1 warnings
                    """
            )
    }

    override fun getDetector(): Detector {
        return NewThreadDetector()
    }

    override fun getIssues(): List<Issue> {
        return listOf(NewThreadDetector.ISSUE)
    }
}