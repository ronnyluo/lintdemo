package com.example.lintdemo

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.base_module.LintBaseModelTestClass
import android.content.Intent
import android.content.BroadcastReceiver
import android.content.IntentFilter

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        LintTestClass()
        LintBaseModelTestClass()
        Log.d("", "")
        Thread()
    }
}

