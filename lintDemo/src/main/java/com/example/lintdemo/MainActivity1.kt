package com.example.lintdemo

import android.graphics.BitmapFactory

import android.graphics.Bitmap

import android.os.AsyncTask
import android.R
import android.view.View
import android.widget.Button


//internal class BitmapWorkerTask :
//    AsyncTask<Int?, Void?, Bitmap>() {
//
//
//    //    ...
//    // 在后台进行图片解码
//    protected override fun doInBackground(vararg params: Int): Bitmap {
//        return BitmapFactory.decodeFile("some path")
//    } //    ...
//}
//
//fun test() {
//    val btnLoadImage: Button = findViewById(R.id.btn) as Button
//    btnLoadImage.setOnClickListener(View.OnClickListener { val bitmap = BitmapFactory.decodeFile("some path") })
//}